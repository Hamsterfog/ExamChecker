package pl.com.tt.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.com.tt.config.CompilerConfig;
import pl.com.tt.model.Project;

@Slf4j
@AllArgsConstructor
public class CompilerImpl implements Compiler {
	
	private CompilerConfig config;

	@Override
	public List<File> compileAllUnderRootFolder(File rootFolder) {
		if(rootFolder.isDirectory()) {
			return compileProjects(FileHelper.getAllSubfolders(rootFolder));
		} else {
			return new ArrayList<>();
		}
	}

	public List<File> compileProjects(List<File> toCompile) {
		List<File> compiledProjects = toCompile.stream()
				.filter(File::isDirectory)
				.map(this::compileProject)
				.filter(Optional::isPresent)
				.map(Optional::get)
				.collect(Collectors.toList());
		return compiledProjects;
	}

	@Override
	public List<Project> compileUnderRootFolder(File rootFolder) {
		if(rootFolder.isDirectory()) {
			return compile(FileHelper.getAllSubfolders(rootFolder));
		} else {
			return new ArrayList<>();
		}
	}

	@Override
	public List<Project> compile(List<File> toCompile) {
		List<Project> projects = new ArrayList<>();
		for(File file : toCompile){
			if(file.isDirectory()) {
				Project project = new Project(file);
				project.setCompiled(compileProject(file).isPresent());
				project.getObjectives().put("Is Compiled", project.isCompiled());
				projects.add(project);
			}
		}
		return projects;
	}

	private Optional<File> compileProject(File file) {
		try {
			if (file != null && file.exists()) {
				Process pro = Runtime.getRuntime().exec(config.getCompileCommand() + file.getAbsolutePath() + File.separator + config.getBuildFileName());
				if(isBuildSuccess(pro.getInputStream())){
					return Optional.of(file);
				}
			}
		} catch (Exception e) {
			log.debug("Could not compile project: "+file.getName());
		}
		return Optional.empty();

	}
	
	private boolean isBuildSuccess(InputStream ins) throws IOException {
	    String line = null;
	    BufferedReader in = new BufferedReader(new InputStreamReader(ins));
	    while ((line = in.readLine()) != null) {
	        if(line.contains("BUILD SUCCESS")){
	        	return true;
	        }
	    }
	    return false;
	  }

}
