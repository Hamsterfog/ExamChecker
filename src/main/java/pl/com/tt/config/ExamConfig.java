package pl.com.tt.config;

import java.util.List;

import pl.com.tt.model.Criteria;

public interface ExamConfig {
	
	List<Criteria> getCriterias();
	
	
}
