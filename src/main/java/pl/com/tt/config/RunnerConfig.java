package pl.com.tt.config;

public interface RunnerConfig {

	String getCompileCommand();
	String getApplicationShortPath();
	
}
