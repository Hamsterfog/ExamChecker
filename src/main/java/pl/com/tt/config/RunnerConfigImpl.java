package pl.com.tt.config;

public class RunnerConfigImpl implements RunnerConfig{

	private String baseCommand;
	private String targetFilePath;
	
	
	public RunnerConfigImpl(String baseCommand, String targetFilePath) {
		this.baseCommand = baseCommand;
		this.targetFilePath = targetFilePath;
	}

	@Override
	public String getCompileCommand() {
		return baseCommand;
	}

	@Override
	public String getApplicationShortPath() {
		return targetFilePath;
	}

}
