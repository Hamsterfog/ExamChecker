package pl.com.tt.parser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import lombok.extern.slf4j.Slf4j;
import pl.com.tt.config.ApplicationConfig;
import pl.com.tt.config.CompilerConfig;
import pl.com.tt.config.CompilerConfigIImpl;
import pl.com.tt.config.ExamConfig;
import pl.com.tt.config.ExamConfigImpl;
import pl.com.tt.config.RunnerConfig;
import pl.com.tt.config.RunnerConfigImpl;
import pl.com.tt.model.Criteria;

@Slf4j
public class XpathParser implements XMLParser {
	
	private static final String CONFIG_NAME = "exam_config.xml"; 

	private static final String CHECKER_PATH = "/checker/runner";
	private static final String COMPILER_PATH = "/checker/compiler";
	private static final String APPLICATION_PATH = "/checker/application";
	private static final String CRITERIA_PATH = "/checker/examConfig/criteria";
	
	private static final String NAME = "name";
	private static final String INPUT = "input";
	private static final String OUTPUT = "output";
	private static final String EXCLUDED_OUTPUT = "excludedOutput";
	private static final String COMMAND = "command";
	private static final String SHORT_PATH = "shortPath";
	private static final String BUILDER_CONFIG_FILE_NAME = "fileName";
	private static final String WORKSPACE_FOLDER_ALL = "workspaceFolder";

	private Document doc;

	public XpathParser() throws ParserConfigurationException, SAXException, IOException {
		File inputFile = new File(CONFIG_NAME);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;

		dBuilder = dbFactory.newDocumentBuilder();

		doc = dBuilder.parse(inputFile);
		doc.getDocumentElement().normalize();
	}

	@Override
	public Optional<CompilerConfig> getCompilerConfig() {
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodeList = (NodeList) xPath.compile(COMPILER_PATH).evaluate(doc, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String compilerName = eElement.getAttribute(NAME);
					String command = eElement.getElementsByTagName(COMMAND).item(0).getTextContent();
					String fileName = eElement.getElementsByTagName(BUILDER_CONFIG_FILE_NAME).item(0).getTextContent();
					return Optional.ofNullable(new CompilerConfigIImpl(compilerName, command, fileName));
				}
			}
		} catch (Exception e) {
			log.debug("Could not parse CompilerConfig from XML");
		}
		return Optional.empty();
	}

	@Override
	public Optional<ApplicationConfig> getApplicationConfig() {
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodeList = (NodeList) xPath.compile(APPLICATION_PATH).evaluate(doc, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String workspaceFolder = eElement.getElementsByTagName(WORKSPACE_FOLDER_ALL).item(0).getTextContent();
					return Optional.ofNullable(new ApplicationConfig(new File(workspaceFolder)));
				}
			}
		} catch (Exception e) {
			log.debug("Could not parse ApplicationConfig from XML");
		}
		return Optional.empty();
	}

	@Override
	public Optional<ExamConfig> getExamConfig() {
		try {
			List<Criteria> criterias = new ArrayList<>();
			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodeList = (NodeList) xPath.compile(CRITERIA_PATH).evaluate(doc, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String criteriaName = eElement.getAttribute(NAME);
					NodeList excludedOutputs = eElement.getElementsByTagName(EXCLUDED_OUTPUT);
					NodeList outputs = eElement.getElementsByTagName(OUTPUT);
					NodeList inputs = eElement.getElementsByTagName(INPUT);
					List<String> inputList = getChildNodesTexts(inputs);
					List<String> outputList = getChildNodesTexts(outputs);
					List<String> excludedOutputList = getChildNodesTexts(excludedOutputs);
					criterias.add(new Criteria(criteriaName, inputList, outputList,excludedOutputList));
				}
			}
			return Optional.ofNullable(new ExamConfigImpl(criterias));
		} catch (Exception e) {
			log.debug("Could not parse ExamConfig from XML");
		}
		return Optional.empty();
	}

	@Override
	public Optional<RunnerConfig> getRunnerConfig() {
		try {
			XPath xPath = XPathFactory.newInstance().newXPath();
			NodeList nodeList = (NodeList) xPath.compile(CHECKER_PATH).evaluate(doc, XPathConstants.NODESET);

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node nNode = nodeList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String command = eElement.getElementsByTagName(COMMAND).item(0).getTextContent();
					String shortPath = eElement.getElementsByTagName(SHORT_PATH).item(0).getTextContent();
					return Optional.ofNullable(new RunnerConfigImpl(command, shortPath));
				}
			}
		} catch (Exception e) {
			log.debug("Could not parse RunnerConfig from XML");
		}
		return Optional.empty();
	}

	private List<String> getChildNodesTexts(NodeList nodeList) {
		List<String> texts = new ArrayList<>();
		for (int j = 0; j < nodeList.getLength(); j++) {
			Node inputNode = nodeList.item(j);
			if (inputNode.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) inputNode;
				texts.add(element.getTextContent());
			}
		}
		return texts;
	}

}
