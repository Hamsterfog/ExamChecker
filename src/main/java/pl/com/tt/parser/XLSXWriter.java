package pl.com.tt.parser;

import java.io.IOException;

public interface XLSXWriter {

	void writeProjectsToXLSXFile() throws IOException;

}
