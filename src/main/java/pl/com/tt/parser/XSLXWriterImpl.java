package pl.com.tt.parser;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.com.tt.model.Project;

@Slf4j
@AllArgsConstructor
public class XSLXWriterImpl implements XLSXWriter {
	
	private static final String FILE_NAME = "Results";
	private static final String EXTENSION = ".xlsx";

	private List<Project> projects;
	private List<String> headerRows;
	
    @Override
    public void writeProjectsToXLSXFile() throws IOException {
		log.debug("XSLX file creation started");
		
        File myFile = createFile();
        XSSFWorkbook myWorkBook = new XSSFWorkbook();
        XSSFSheet mySheet = myWorkBook.createSheet(FILE_NAME);
        log.debug("Sheet created with name: "+FILE_NAME);
        
        mySheet = fillHeader(mySheet, headerRows);
        log.debug("Header filled with data");
        
        int rownum = 1;
        for (Project project : projects) {
            int cellnum = 0;
            Row row = mySheet.createRow(rownum++);
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue(project.getStudent().getName());
            cell = row.createCell(cellnum++);
            cell.setCellValue(project.getStudent().getSurname());
            cell = row.createCell(cellnum++);
            cell.setCellValue(project.isCompiled());
            for (String rowKey : headerRows) {
                cell = row.createCell(cellnum++);
                cell.setCellValue(project.getObjectives().get(rowKey));
            }
        }
        log.debug("Data filled");
        
        for (int i = 0; i < headerRows.size() + 3; i++) {
            mySheet.autoSizeColumn(i);
        }
        log.debug("Columns formatted");
        FileOutputStream os = new FileOutputStream(myFile);
        myWorkBook.write(os);
        myWorkBook.close();
    }

	private static File createFile() throws IOException {
		File myFile = new File(FILE_NAME+EXTENSION);
        if (myFile.exists()) {
            myFile.delete();
        }
        myFile.createNewFile(); 
		return myFile;
	}

    private XSSFSheet fillHeader(XSSFSheet mySheet, List<String> additionalRows) {
        Row row = mySheet.createRow(mySheet.getLastRowNum());

        List<String> headerList = new ArrayList<>();
        headerList.add("Imie");
        headerList.add("Nazwisko");
        headerList.add("is Compiled?");
        headerList.addAll(additionalRows);
        fillRow(row, headerList);
        return mySheet;
    }

    private Row fillRow(Row row, List<String> values) {
        int cellnum = 0;
        for (String value : values) {
            Cell cell = row.createCell(cellnum++);
            cell.setCellValue(value);
        }
        return row;
    }
}
