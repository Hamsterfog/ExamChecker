package pl.com.tt.model;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class Project {

    private File projectRoot;
    private Student student;
    private boolean isCompiled;
    private Map<String,Boolean> objectives;

    public Project() {
    	objectives = new HashMap<>();
    }

    public Project(File projectRoot) {
        this.projectRoot = projectRoot;
        objectives = new HashMap<>();
        String[] studentCred = projectRoot.getName().split("(?=\\p{Upper})");
        if(studentCred.length >1){
        	student = new Student(studentCred[0],studentCred[1]);
        } else {
        	student = new Student(studentCred[0],"");
        }
    }

    public Project(File projectRoot, Student student, boolean isCompiled, Map<String, Boolean> objectives) {
        this.projectRoot = projectRoot;
        this.student = student;
        this.isCompiled = isCompiled;
        this.objectives = objectives;
    }

    public File getProjectRoot() {
        return projectRoot;
    }

    public void setProjectRoot(File projectRoot) {
        this.projectRoot = projectRoot;
        String[] studentCred = projectRoot.getName().split("(?=\\p{Upper})");
        student = new Student(studentCred[0],studentCred[1]);
    }

}
